/*
** init_redir.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell2/v1/minishell2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Feb 26 18:32:50 2015 lionel karmes
** Last update Sun Mar 15 15:15:33 2015 lionel karmes
*/

#include "my.h"

int	char_redirection(char c)
{
  if (c == '|' || c == '<' || c == '>')
    return (1);
  return (0);
}

int    option_open(char *cmds, int i)
{
  if (cmds[i] == '>' && cmds[i + 1] == '>')
    return (O_APPEND | O_WRONLY | O_CREAT);
  else if (cmds[i] == '>')
    return (O_TRUNC | O_WRONLY | O_CREAT);
  else if (cmds[i] == '<' && cmds[i + 1] == '<')
    return (DOUBLE_LEFT);
  else if (cmds[i] == '<')
    return (O_RDONLY);
  return (NOT_OPTION);
}

int	init_list_openfile_next(t_list **list_openfile, char *cmds,
				int i, int option)
{
  t_openfile	*openfile;
  int		len_path;
  static int	id = 0;
  char		*path;

  if ((*list_openfile)->size == 0)
    id = 0;
  id++;
  len_path = 0;
  if (!(openfile = pmalloc(sizeof(t_openfile))))
    return (-1);
  openfile->id = id;
  openfile->option = option;
  while (!char_redirection(cmds[i + len_path]) &&
	 !space_or_tab(cmds[i + len_path]) && cmds[i + len_path])
      len_path++;
  if (!(path = pmalloc(sizeof(char) * (len_path + 1))))
    return (-1);
  my_strncpy2(path, cmds, i, len_path);
  openfile->path = path;
  my_put_in_list_end(list_openfile, openfile);
  return (1);
}
