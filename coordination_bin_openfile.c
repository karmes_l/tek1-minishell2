/*
** init_redir.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell2/v1/minishell2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Feb 26 18:32:50 2015 lionel karmes
** Last update Wed Mar 11 17:08:23 2015 lionel karmes
*/

#include "my.h"

void	coordination_bin_openfile_next(t_bin *bin, t_openfile **openfile,
				       int option, int *i)
{
  if (option == O_RDONLY)
    {
      bin->entry = *openfile;
      bin->double_left = NULL;
      *openfile = (*openfile)->e_next;
    }
  if (option == (O_APPEND | O_WRONLY | O_CREAT) ||
      option == (O_TRUNC | O_WRONLY | O_CREAT))
    {
      if (option == (O_APPEND | O_WRONLY | O_CREAT))
	(*i)++;
      bin->sortie = *openfile;
      *openfile = (*openfile)->e_next;
    }
}

char	*parsing_double_left(char *cmds, int i)
{
  int	len_path;
  char	*path;

  i += 2;
  len_path = 0;
  if (space_or_tab(cmds[i]))
    i++;
  while (!char_redirection(cmds[i + len_path]) &&
	 !space_or_tab(cmds[i + len_path]) && cmds[i + len_path])
    len_path++;
  if (!(path = pmalloc(sizeof(char) * (len_path + 1))))
    return (NULL);
  my_strncpy2(path, cmds, i, len_path);
  return (path);
}

int	coordination_bin_openfile(t_bin *bin, t_openfile *openfile, char *cmds)
{
  int	i;
  int	option;
  char	*double_left;

  i = 0;
  while (cmds[i])
    {
      if (cmds[i] == '|')
	bin = bin->e_next;
      if (bin == NULL)
	return (1);
      option = option_open(cmds, i);
      if (option == DOUBLE_LEFT)
	{
	  if ((double_left = parsing_double_left(cmds, i)) == NULL)
	    return (-1);
	  bin->entry = NULL;
	  bin->double_left = double_left;
	  i++;
	}
      else
	coordination_bin_openfile_next(bin, &openfile, option, &i);
      i++;
    }
  return (1);
}
