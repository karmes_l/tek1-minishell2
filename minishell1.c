/*
** minishell.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sun Jan 11 17:28:16 2015 lionel karmes
** Last update Sun Feb  1 17:36:15 2015 lionel karmes
*/

#include "my.h"

sig_atomic_t	g_signal_int = 0;

void	free_environ(t_env *environ)
{
  if (environ->path != NULL)
    free_tab(environ->path);
  if (environ->home != NULL)
    free(environ->home);
  if (environ->pwd != NULL)
    free(environ->pwd);
  if (environ->oldpwd != NULL)
    free(environ->oldpwd);
  if (environ->env != NULL)
    free_tab(environ->env);
}

void	minishell1(char **env)
{
  t_env	environ;

  init_environ(&environ, env);
  read_command(&environ);
  free_environ(&environ);
}
