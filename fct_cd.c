/*
** fct_cd_exit.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Jan 12 18:37:42 2015 lionel karmes
** Last update Mon Mar  9 11:19:31 2015 lionel karmes
*/

#include "my.h"

void		load_pwd(char **path, char *current, char *filename)
{
  if (filename[0] == '/')
    *path = my_strdup(filename);
  else if (current != NULL)
    {
      if (filename[0] == '.')
	{
	  while (filename[0] == '.')
	    {
	      while (!my_strncmp("..", filename, 2))
		{
		  swap_str_left(filename, 2);
		  path_parent(current);
		}
	      while (!my_strncmp(".", filename, 1))
		swap_str_left(filename, 1);
	      while (!my_strncmp("/", filename, 1))
		swap_str_left(filename, 1);
	    }
	  new_element(path, current, "", filename);
	}
      else
	new_element(path, current, "/", filename);
    }
  else
    *path = NULL;
}

void		build_tab_args_cd(char ***args, char *name, char *value)
{
  *args = malloc(sizeof(char *) * (4));
  (*args)[0] = my_strdup("setenv");
  (*args)[1] = my_strdup(name);
  (*args)[2] = my_strdup(value);
  (*args)[3] = NULL;
}

void		fct_cd_update_env(t_env *environ, char *filename)
{
  char		**args;
  char		*pwd;
  char		*tmp;

  tmp =  my_strdup(environ->pwd);
  load_pwd(&pwd, tmp, filename);
  if (environ->pwd != NULL)
    {
      build_tab_args_cd(&args, "OLDPWD", environ->pwd);
      fct_setenv(environ, args, my_strtablen(args));
      free_tab(args);
    }
  if (pwd != NULL)
    {
      build_tab_args_cd(&args, "PWD", pwd);
      free(pwd);
      fct_setenv(environ, args, my_strtablen(args));
      free_tab(args);
    }
  if (tmp != NULL)
    free(tmp);
}

void		fct_cd_next(t_env *environ, char *filename)
{
  if (filename == NULL)
    {
      my_putstrerror("[ERROR] : variable HOME doesn't exist ");
      my_putstrerror("in the environnement\n");
    }
  else if (chdir(filename) == -1)
    error_path(filename);
  else
    fct_cd_update_env(environ, filename);
}

int		fct_cd(t_env *environ, char **args, int ac)
{
  if (ac > 1 && !my_strcmp("-", args[1]))
    {
      if (environ->oldpwd != NULL)
	fct_cd_next(environ, environ->oldpwd);
      else
	{
	  my_putstrerror("[ERROR] : variable OLDPWD doesn't exist ");
	  my_putstrerror("in the environnement\n");
	}
    }
  else if (ac == 1 || (ac > 1 && !my_strcmp("~", args[1])))
    fct_cd_next(environ, environ->home);
  else
    fct_cd_next(environ, args[1]);
  return (1);
}
