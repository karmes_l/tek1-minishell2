/*
** find_bin.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Jan 14 14:44:00 2015 lionel karmes
** Last update Sun Mar 15 15:18:43 2015 lionel karmes
*/

#include "my.h"

int		find_bin(char *path, char *file_name)
{
  DIR		*dir;
  struct dirent	*entry;

  if ((dir = opendir(path)) == NULL)
    return (0);
  while ((entry = readdir(dir)) != NULL)
    {
      if (!my_strcmp(entry->d_name, file_name))
	{
	  closedir(dir);
	  return (1);
	}
    }
  closedir(dir);
  return (0);
}

int		exec_bin(char *filename, char **args, char **env)
{
  pid_t		pid;
  int		status;

  status = 1;
  if ((pid = fork()) == -1)
    {
      my_putstrerror("[ERROR] : fork");
      exit(2);
    }
  if (pid == 0)
    {
      if (execve(filename, args, env) == -1)
	exit(1);
    }
  if (wait(&status) == -1)
    {
      kill(pid, SIGKILL);
      wait(&status);
    }
  if (WEXITSTATUS(status) == 1)
    return (0);
  return (1);
}
