/*
** fct_env.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Jan 12 18:29:20 2015 lionel karmes
** Last update Mon Mar  9 11:27:18 2015 lionel karmes
*/

#include "my.h"

void	new_element(char **str, char *arg1, char *separator, char *arg2)
{
  if ((*str = pmalloc(sizeof(char) *
		      (my_strlen(arg1) + my_strlen(separator)
		       + my_strlen(arg2) + 2))) == NULL)
    exit(1);
  my_strcpy(*str, arg1);
  my_strcat(*str, separator);
  my_strcat(*str, arg2);
}

int	fct_setenv(t_env *environ, char **args, int ac)
{
  int	i;

  i = 0;
  if (ac < 3)
    {
      my_putstrerror("[ERROR] : setenv name value\n");
      return (1);
    }
  update_environ(environ, args[1], args[2]);
  while (environ->env[i] != NULL)
    {
      if (!my_strncmp(args[1], environ->env[i], my_strlen(args[1]))
	  && environ->env[i][my_strlen(args[1])] == '=')
	{
	  free(environ->env[i]);
	  new_element(&(environ->env[i]), args[1], "=", args[2]);
	  return (1);
	}
      i++;
    }
  environ->env = char2_tab_realloc(environ->env, i + 1);
  new_element(&(environ->env[i]), args[1], "=", args[2]);
  return (1);
}

int	fct_unsetenv(t_env *environ, char **args, int ac)
{
  int	i;

  i = 0;
  if (ac < 2)
    {
      my_putstrerror("[ERROR] : unsetenv name\n");
      return (1);
    }
  update_environ(environ, args[1], NULL);
  while (environ->env[i] != NULL)
    {
      if (!my_strncmp(args[1], environ->env[i], my_strlen(args[1]))
	  && environ->env[i][my_strlen(args[1])] == '=')
	{
	  remove_element_char2_tab(&(environ->env), i);
	  return (1);
	}
      i++;
    }
  error_env_notfound(args[1]);
  return (1);
}

int	fct_env(char **env, char **args, int ac)
{
  int	i;

  i = 0;
  if (ac > 1)
    if (error_env_args(args[1]))
      return (1);
  while (env[i] != NULL)
    {
      my_putstr(env[i]);
      if (ac == 1)
	my_putchar('\n');
      i++;
    }
  return (1);
}
