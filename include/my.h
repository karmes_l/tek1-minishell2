/*
** my.h for  in /home/karmes_l/test/tmp_Piscine_C_J09
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Oct  9 15:22:30 2014 lionel karmes
** Last update Sun Mar 15 09:40:32 2015 lionel karmes
*/

#ifndef MY_H_
# define MY_H_

# include <features.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include <sys/types.h>
# include <dirent.h>
# include <signal.h>
# include <stdlib.h>
# include <unistd.h>
# include "minishell.h"

# define MAX(v1, v2)	((v1) > (v2)) ? (v1) : (v2)

char		*convert_base(char *, char *, char *);
int		count_num(long nb);
int		my_charisnum(char);
void		my_putchar(char);
void		my_putchar0(char);
void		my_putcharerror(char);
void		my_putnbr(long);
void		my_swap(int *, int *);
void		my_putstr(char *);
void		my_putstr0(char *);
void		my_putstrerror(char *);
int		my_strlen(char *);
int		my_getnbr(char *);
void		my_sort_int_tab(int *, int);
char		*my_strcpy(char *, char *);
char		*my_strncpy(char *, char *, int);
char		*my_strncpy2(char *, char *, int, int);
char		*my_revstr(char *);
char		*my_strstr(char *, char *);
char		*my_strdup(char *);
int		my_strcmp(char *, char *);
int		my_strncmp(char *, char *, int n);
char		*my_strupcase(char *);
char		*my_strlowcase(char *);
char		*mystrcapitalize(char *);
int		my_str_isalpha(char *);
int		my_str_isnum(char *);
int		mu_str_islower(char *);
int		my_str_isupper(char *);
int		my_str_isprintable(char *);
char		*my_strcat(char *, char *);
char		*my_strncat(char *, char *, int);
int		my_strlcat(char *, char *, int);
int		my_str_isnum2(char *);
unsigned long	pow_10(int);
int		power(unsigned long, unsigned long);
int		my_putchar2(int);
char		**my_str_to_wordtab(char *, char);
void		*pmalloc(int);
char		*get_next_line(int);
void		minishell1(char **);
void		print_path(char **);
void		read_command(t_env *);
void		read_signal();
void		epure_str(char *);
int		fct_cd(t_env *, char **, int);
int		fct_setenv(t_env *, char **, int);
int		fct_unsetenv(t_env *, char **, int);
int		fct_env(char **, char **, int);
int		fct_exit(char **);
int		my_strtablen(char **);
void		free_tab(char **);
void		parser_path(char ***, char *);
void		parser_home(char **, char *);
void		init_environ(t_env *, char **);
int		find_bin(char *, char *);
int		exec_bin(char *, char **, char **);
void		error_notfound(char *);
void		error_path(char *);
void		error_env_notfound(char *);
int		error_env_args(char *);
void		new_element(char **, char *, char *, char *);
char		**char2_tab_realloc(char **, int);
void		remove_element_char2_tab(char ***, int);
int		kill(pid_t, int);
void		swap_str_left(char *, int);
void		path_parent(char *);
void		update_environ(t_env *, char *, char *);
void		my_put_in_list_start(t_list **, void *);
void		my_put_in_list_end(t_list **, void *);
t_list		*new_list();
void		remove_list(t_list **);
int		init_redir(t_redir *, char **);
void		put_end_element_openfile(t_list **, t_openfile *);
void		put_end_element_bin(t_list **, t_bin *);
void		put_start_element_openfile(t_list **, t_openfile *);
void		put_start_element_bin(t_list **, t_bin *);
void		remove_list_bin(t_bin *);
void		remove_list_openfile(t_openfile *);
int		space_or_tab(char);
int		option_open(char *, int);
int		valid_redirection_file(char *, int *);
int		char_redirection(char);
int		msg_error_redirection(char);
int		msg_error_redirection_str(char *);
int		init_list_openfile_next(t_list **, char *, int, int);
void		print_list_openfile(t_list **);
void		print_list_bin(t_list **);
int		init_list_bin_next(t_list **, char *, int *);
int		valid_cmds(char **);
int		valid_redirection(char *);
int		update_cmds(char **);
void		free_redir(t_redir *);
char		*char1_tab_realloc(char *, int);
int		coordination_bin_openfile(t_bin *, t_openfile *, char *);
int		exec_cmds(t_redir *, t_env *);
int		find_command(t_env *, char **, int);
int		find_command_next(t_env *, char **);
int		msg_error_open(char *, int);
int		redirection(t_bin *);
int		read_command_next(char *, t_env *);
int		several_pipes(t_env *, t_redir *);
int		init_pipe(int []);

#endif /* !MY_H_ */

