/*
** events.h for  in /home/karmes_l/Projets/Igraph/Wolf3D/v2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Dec 20 11:47:45 2014 lionel karmes
** Last update Sat Mar  7 12:31:51 2015 lionel karmes
*/

#ifndef MINISHELL_H_
# define MINISHELL_H_

typedef struct		s_env
{
  char			**path;
  char			*home;
  char			*pwd;
  char			*oldpwd;
  char			**env;
}			t_env;

extern sig_atomic_t	g_signal_int;

typedef struct		s_openfile
{
  int			id;
  char			*path;
  int			option;
  struct s_openfile	*e_next;
  struct s_openfile	*e_prev;
}			t_openfile;

typedef struct		s_bin
{
  int			id;
  char			*name;
  t_openfile		*entry;
  t_openfile		*sortie;
  char			*double_left;
  struct s_bin		*e_next;
  struct s_bin		*e_prev;
}			t_bin;

typedef struct		s_list
{
  int			size;
  void			*l_start;
  void			*l_end;
  int			what_element;
}			t_list;

typedef struct		s_redir
{
  t_list		*list_bin;
  t_list		*list_openfile;
}			t_redir;

# define END	(2)

# define BIN	(1)
# define OPENFILE	(2)
 
# define DOUBLE_LEFT	(4)
# define NOT_OPTION	(5)

#endif /* !MINISHELL_H_ */
