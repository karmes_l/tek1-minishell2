/*
** read_command.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Jan 12 17:15:51 2015 lionel karmes
** Last update Sat Mar 14 15:10:35 2015 lionel karmes
*/

#include "my.h"

void	get_sigint(int sign)
{
  g_signal_int = sign;
}

void	get_sigquit(int sign)
{
  g_signal_int = sign;
}

void	get_sigtstp(int sign)
{
  g_signal_int = sign;
}

void	read_signal()
{
  g_signal_int = 0;
  if (signal(SIGINT, get_sigint) == SIG_ERR)
    my_putstrerror("[ERROR] : SIGINT\n");
  if (signal(SIGQUIT, get_sigquit) == SIG_ERR)
    my_putstrerror("[ERROR] : SIGQUIT\n");
  if (signal(SIGTSTP, get_sigtstp) == SIG_ERR)
    my_putstrerror("[ERROR] : SIGTSTP\n");
}
