/*
** read_command.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Jan 12 17:15:51 2015 lionel karmes
** Last update Sat Mar 14 19:03:08 2015 lionel karmes
*/

#include "my.h"

int		not_pipe(t_env *environ, t_redir *redir)
{
  t_bin		*bin;
  char		**args;
  int		ac;

  bin = redir->list_bin->l_start;
  if (redirection(bin) != -1)
    {
      args = my_str_to_wordtab(bin->name, ' ');
      ac = my_strtablen(args);
      if (find_command(environ, args, ac) == END)
	{
	  free_tab(args);
	  return (END);
	}
      free_tab(args);
    }
  return (1);
}

int		exec_cmds(t_redir *redir, t_env *environ)
{
  int		oldfd_in;
  int		oldfd_out;

  oldfd_in = dup(0);
  oldfd_out = dup(1);
  if (redir->list_bin->size == 1)
    {
      if (not_pipe(environ, redir) == END)
	return (END);
    }
  else
    several_pipes(environ, redir);
  dup2(oldfd_in, 0);
  dup2(oldfd_out, 1);
  return (1);
}

int		read_command_next(char *buff, t_env *environ)
{
  char		**commands;
  int		i;
  t_redir	redir;

  i = 0;
  commands = my_str_to_wordtab(buff, ';');
  while (commands != NULL && commands[i] != NULL)
    {
      epure_str(commands[i]);
      if (init_redir(&redir, &(commands[i])) == -1)
	{
	  free_redir(&redir);
	  break;
	}
      if (exec_cmds(&redir, environ) == END)
	{
	  free_redir(&redir);
	  free_tab(commands);
	  return (END);
	}
      free_redir(&redir);
      i++;
    }
  free_tab(commands);
  return (0);
}
