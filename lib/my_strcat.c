/*
** my_strcat.c for  in /home/karmes_l/test/tmp_Piscine_C_J07
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Oct  7 17:00:16 2014 lionel karmes
** Last update Wed Jan 14 16:23:11 2015 lionel karmes
*/

char	*my_strcat(char *dest, char *src)
{
  int	j;
  int	i;

  i = 0;
  j = 0;
  while (dest[j] != '\0')
    {
      j = j + 1;
    }
  while (src[i] != '\0')
    {
      *(dest + j + i) = *(src + i);
      i = i + 1;
    }
  dest[i + j] = '\0';
  return (dest);
}
