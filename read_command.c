/*
** read_command.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Jan 12 17:15:51 2015 lionel karmes
** Last update Sun Mar 15 10:23:02 2015 lionel karmes
*/

#include "my.h"

int	find_command_next(t_env *environ, char **args)
{
  int	i;
  char	*file_name;

  i = 0;
  if (exec_bin(args[0], args, environ->env))
    return (1);
  if (environ->path == NULL)
    return (0);
  while (environ->path[i] != 0)
    {
      if (find_bin(environ->path[i], args[0]))
	{
	  new_element(&file_name, environ->path[i], "/", args[0]);
	  exec_bin(file_name, args, environ->env);
	  free(file_name);
	  return (1);
	}
      i++;
    }
  return (0);
}

int	find_command(t_env *environ, char **args, int ac)
{
  int	cmd_found;

  cmd_found = 0;
  cmd_found = (!my_strcmp("cd", args[0])) ?
    fct_cd(environ, args, ac) : cmd_found;
  cmd_found = (!my_strcmp("setenv", args[0])) ?
    fct_setenv(environ, args, ac) : cmd_found;
  cmd_found = (!my_strcmp("unsetenv", args[0])) ?
    fct_unsetenv(environ, args, ac) : cmd_found;
  cmd_found = (!my_strcmp("env", args[0])) ?
    fct_env(environ->env, args, ac) : cmd_found;
  cmd_found = (!my_strcmp("exit", args[0])) ? 2 : cmd_found;
  if (!cmd_found)
    if (!find_command_next(environ, args))
      error_notfound(args[0]);
  if (cmd_found == END)
    return (END);
  return (1);
}

void	read_command(t_env *environ)
{
  char	*buff;
  int	end;

  end = 0;
  while (!end)
    {
      my_putstr("?>");
      read_signal();
      buff = get_next_line(0);
      if (buff == NULL && (g_signal_int != SIGINT &&
			   g_signal_int != SIGQUIT && g_signal_int != SIGTSTP))
	break;
      else if (g_signal_int == SIGINT || g_signal_int == SIGQUIT ||
	       g_signal_int == SIGTSTP)
	my_putchar('\n');
      end = read_command_next(buff, environ);
      free(buff);
    }
}
