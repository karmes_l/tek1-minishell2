/*
** my_strcpy2.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell2/v1/minishell2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Mar  2 18:33:11 2015 lionel karmes
** Last update Mon Mar  2 18:39:20 2015 lionel karmes
*/

void	my_strncpy2(char *dest, char *src, int i, int n)
{
  int	j;

  j = 0;
  while (src[i + j] && j < n)
    {
      dest[j] = src[i + j];
      j++;
    }
  dest[j] = '\0';
}
