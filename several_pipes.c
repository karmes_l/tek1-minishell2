/*
** several_pipes.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell2/v1/minishell2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Mar 11 17:35:02 2015 lionel karmes
** Last update Sun Mar 15 15:17:11 2015 lionel karmes
*/

#include "my.h"

void		process_son(int fildes[2], t_env *environ, t_bin *bin, int num_bin)
{
  char		**args;
  int		ac;
  int		valid_redirection;

  read_signal();
  args = my_str_to_wordtab(bin->name, ' ');
  ac = my_strtablen(args);
  close(fildes[0]);
  if (num_bin > 0)
    dup2(fildes[1], 1);
  valid_redirection = redirection(bin);
  find_command(environ, args, ac);
  if (valid_redirection == -1)
    {
      close(fildes[1]);
      free_tab(args);
      exit(1);
    }
  close(fildes[1]);
  free_tab(args);
  exit(0);
}

int		process_daddy(int fildes[2], int num_bin, int pid)
{
  int		status;

  close(fildes[1]);
  if (wait(&status) == -1)
    {
      if (g_signal_int == SIGTSTP || g_signal_int == SIGQUIT)
  	kill(pid, SIGKILL);
      wait(&status);
    }
  if (WEXITSTATUS(status) == 1)
    {
      close(fildes[0]);
      return (0);
    }
  if (num_bin > 0)
    dup2(fildes[0], 0);
  close(fildes[0]);
  return (1);
}

int		init_pipe(int fildes[2])
{
  if (pipe(fildes) == -1)
    {
      my_putstrerror("[ERROR] : pipe\n");
      return (-1);
    }
  return (1);
}

int		several_pipes(t_env *environ, t_redir *redir)
{
  int		i;
  int		fildes[2];
  int		pid;
  t_bin		*bin;

  i = 1;
  bin = redir->list_bin->l_start;
  while (bin != NULL)
    {
      if (init_pipe(fildes) == -1)
	return (-1);
      if ((pid = fork()) == -1)
	{
	  my_putstrerror("[ERROR] : fork\n");
	  return (-1);
	}
      if (pid == 0)
	process_son(fildes, environ, bin, redir->list_bin->size - i);
      if (process_daddy(fildes, redir->list_bin->size - i, pid) == 0)
      	break;
      i++;
      bin = bin->e_next;
    }
  return (1);
}
