/*
** epure_str.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Jan 12 17:33:31 2015 lionel karmes
** Last update Sun Mar 15 15:19:12 2015 lionel karmes
*/

#include "my.h"

void	decal_str(char *str, int i)
{
  while (str[i] != '\0')
    {
      str[i] = str[i + 1];
      i++;
    }
}

int	space_or_tab(char c)
{
  if (c == ' '  || c == '\t')
    return (1);
  return (0);
}

void	epure_str(char *str)
{
  int	i;

  i = 0;
  if (str != NULL)
    {
      while (str[i] != '\0')
	{
	  if (i == 0 && space_or_tab(str[i]))
	    decal_str(str, i);
	  else if (space_or_tab(str[i]) &&
		   (space_or_tab(str[i + 1]) || str[i + 1] == '\0'))
	    decal_str(str, i);
	  else
	    i++;
	}
    }
}
