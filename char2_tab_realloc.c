/*
** char2_tab_realloc.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell1/v2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Jan 15 17:59:24 2015 lionel karmes
** Last update Sat Mar  7 09:47:28 2015 lionel karmes
*/

#include "my.h"

char	**char2_tab_realloc(char **tab, int size)
{
  int	i;
  char	**new_tab;

  i = 0;
  if ((new_tab = malloc(sizeof(char *) * (size + 1))) == NULL)
    exit(1);
  new_tab[size] = NULL;
  while (tab[i] != NULL && i < size)
    {
      if ((new_tab[i] = malloc(sizeof(char) *
			       (my_strlen(tab[i]) + 1))) == NULL)
	exit(1);
      my_strcpy(new_tab[i], tab[i]);
      i++;
    }
  free_tab(tab);
  return (new_tab);
}

void	remove_element_char2_tab(char ***tab, int index)
{
  int	i;
  int	j;
  int	size;
  char	**new_tab;

  i = 0;
  j = 0;
  size = my_strtablen(*tab);
  if ((new_tab = malloc(sizeof(char *) * (size))) == NULL)
    exit(1);
  new_tab[size - 1] = NULL;
  while (i < size - 1)
    {
      if (i == index)
	j++;
      if ((new_tab[i] = malloc(sizeof(char) * (my_strlen((*tab)[j]) + 1))) ==
	  NULL)
	exit(1);
      my_strcpy(new_tab[i], (*tab)[j]);
      i++;
      j++;
    }
  free_tab(*tab);
  *tab = new_tab;
}

char	*char1_tab_realloc(char *tab, int size)
{
  int	i;
  char	*new_tab;

  i = 0;
  if ((new_tab = malloc(sizeof(char) * (size + 1))) == NULL)
    return (NULL);
  while (tab[i] != '\0' && i < size)
    {
      new_tab[i] = tab[i];
      i++;
    }
  new_tab[i] = '\0';
  free(tab);
  return (new_tab);
}
