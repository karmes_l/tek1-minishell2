/*
** init_redir_next.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell2/v1/minishell2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Mar  2 12:59:02 2015 lionel karmes
** Last update Sat Mar  7 12:31:39 2015 lionel karmes
*/

#include "my.h"

void		print_list_openfile(t_list **list_openfile)
{
  t_openfile	*element;

  if (*list_openfile != NULL)
    {
      my_putstr("\nLIST_OPENFILE\n\n");
      element = (*list_openfile)->l_start;
      while (element != NULL)
	{
	  my_putstr("ID : ");
	  my_putnbr(element->id);
	  my_putstr("\nPATH : ");
	  my_putstr(element->path);
	  my_putstr("\nOPTION : ");
	  my_putnbr(element->option);
	  my_putchar('\n');
	  element = element->e_next;
	}
    }
}

void		print_list_bin(t_list **list_bin)
{
  t_bin		*element;

  if (*list_bin != NULL)
    {
      my_putstr("\nLIST_BIN\n\n");
      element = (*list_bin)->l_start;
      while (element != NULL)
	{
	  my_putstr("ID : ");
	  my_putnbr(element->id);
	  my_putstr("\nNAME : ");
	  my_putstr(element->name);
	  my_putstr("\nENTRY : ");
	  if (element->entry != NULL)
	    my_putstr("YES");
	  my_putstr("\nSORTIE : ");
	  if (element->sortie != NULL)
	    my_putstr("YES");
	  my_putstr("\nDOUBLE_LEFT : ");
	  if (element->double_left != NULL)
	    my_putstr(element->double_left);
	  my_putchar('\n');
	  element = element->e_next;
	}
    }
}
