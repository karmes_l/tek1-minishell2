/*
** message_redir.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell2/v1/minishell2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Mar  2 09:16:20 2015 lionel karmes
** Last update Sun Mar 15 09:40:43 2015 lionel karmes
*/

#include "my.h"

int	msg_error_redirection(char c)
{
  my_putstrerror("bash: syntax error near unexpected token `");
  if (c == '\0')
    my_putstrerror("newline");
  else
    my_putcharerror(c);
  my_putstrerror("'\n");
  return (-1);
}

int	msg_error_redirection_str(char *str)
{
  if (str == NULL)
    {
      my_putstrerror("bash: syntax error : unexpected end of file\n");
      return (-1);
    }
  return (1);
}

int	msg_error_open(char *str, int option)
{
  my_putstrerror("bash: ");
  my_putstrerror(str);
  if (option == O_RDONLY)
    {
      if (access(str, F_OK) == 0)
	my_putstrerror(" Permission denied\n");
      else
	my_putstrerror(" No such file or directory\n");
    }
  else
    my_putstrerror(" Permission denied\n");
  return (-1);
}
