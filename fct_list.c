/*
** init_list.c for  in /home/karmes_l/Projets/Prog_Elem/PushSwap/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec  9 14:41:33 2014 lionel karmes
** Last update Sun Mar 15 15:15:17 2015 lionel karmes
*/

#include "my.h"

void			my_put_in_list_end(t_list **list, void *element)
{
  t_bin			*bin;
  t_openfile		*openfile;

  if ((*list)->what_element == BIN)
    {
      bin = element;
      put_end_element_bin(list, bin);
    }
  else if ((*list)->what_element == OPENFILE)
    {
      openfile = element;
      put_end_element_openfile(list, openfile);
    }
  else
    my_putstrerror("[ERROR] improbable : what_element ?\n");
}

void		my_put_in_list_start(t_list **list, void *element)
{
  t_bin			*bin;
  t_openfile		*openfile;

  if ((*list)->what_element == BIN)
    {
      bin = element;
      put_start_element_bin(list, bin);
    }
  else if ((*list)->what_element == OPENFILE)
    {
      openfile = element;
      put_start_element_openfile(list, openfile);
    }
  else
    my_putstrerror("[ERROR] improbable : what_element ?\n");
}

t_list		*new_list()
{
  t_list	*list;

  if ((list = malloc(sizeof(t_list))) == NULL)
    exit(0);
  list->size = 0;
  list->l_start = NULL;
  list->l_end = NULL;
  return (list);
}

void		remove_list(t_list **list)
{
  t_bin		*bin;
  t_openfile	*openfile;

  if (*list != NULL)
    {
      if ((*list)->what_element == BIN)
	{
	  bin = (*list)->l_start;
	  remove_list_bin(bin);
	}
      else if ((*list)->what_element == OPENFILE)
	{
	  openfile = (*list)->l_start;
	  remove_list_openfile(openfile);
	}
      else
	my_putstrerror("[ERROR improbable ; what_element ?\n");
      free(*list);
      *list = NULL;
    }
}
