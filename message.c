/*
** message.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Jan 15 10:34:18 2015 lionel karmes
** Last update Fri Mar 13 13:38:14 2015 lionel karmes
*/

#include "my.h"

void	error_notfound(char *str)
{
  my_putstr(str);
  my_putstr(": command not found\n");
}

void	error_path(char *str)
{
  my_putstrerror("[ERROR] : chdir : '");
  my_putstrerror(str);
  if (access(str, F_OK) == -1)
    my_putstrerror("' doesn't exist : No such file or directory\n");
  else
    my_putstrerror("' can't be open : Permission denied\n");
}

void	error_env_notfound(char *str)
{
  my_putstrerror("The variable '");
  my_putstrerror(str);
  my_putstrerror("' doesn't exit in the environnement\n");
}

int	error_env_args(char *str)
{
  if (str[0] != '-')
    {
      my_putstrerror("env: invalid arg '");
      my_putstrerror(str);
    }
  else if (str[1] != '0')
    {
      my_putstrerror("env: invalid option '");
      my_putstrerror(str);
    }
  else
    return (0);
  my_putstrerror("'\n");
  return (1);
}
