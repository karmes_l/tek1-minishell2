/*
** remove_element.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell2/v1/minishell2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Feb 26 20:12:07 2015 lionel karmes
** Last update Sun Mar 15 15:14:54 2015 lionel karmes
*/

#include "my.h"

void		remove_list_bin(t_bin *tmp)
{
  t_bin		*element;

  while (tmp != NULL)
    {
      element = tmp;
      free(tmp->name);
      if (tmp->double_left != NULL)
	free(tmp->double_left);
      tmp = tmp->e_next;
      free(element);
    }
}

void		remove_list_openfile(t_openfile *tmp)
{
  t_openfile	*element;

  while (tmp != NULL)
    {
      element = tmp;
      free(tmp->path);
      tmp = tmp->e_next;
      free(element);
    }
}
