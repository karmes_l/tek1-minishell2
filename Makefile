
##
## Makefile for  in /home/karmes_l/Projets/Maths
## 
## Made by lionel karmes
## Login   <karmes_l@epitech.net>
## 
## Started on  Mon Nov  3 16:51:51 2014 lionel karmes
## Last update Sun Mar 15 14:53:02 2015 lionel karmes
##

CC	= gcc

RM	= rm -f

CFLAGS	+= -Wextra -Wall -Werror
CFLAGS	+= -ansi -pedantic
CFLAGS	+= -I./include/

LDFLAGS	= -L./lib/ -lmy

NAME	= mysh

LIB	= lib/

LIBNAME	= $(addprefix $(LIB), libmy.a)

SRCS	= main.c \
	minishell1.c \
	init_environ.c \
	epure_str.c \
	check_signal.c \
	read_command.c \
	read_command_next.c \
	fct_cd.c \
	fct_env.c \
	update_environ.c \
	my_strtablen.c \
	find_bin.c \
	message.c \
	char2_tab_realloc.c \
	swap_str.c \
	fct_list.c \
	put_element.c \
	init_redir.c \
	valid_cmds.c \
	init_list_openfile.c \
	init_list_bin.c \
	coordination_bin_openfile.c \
	print_list.c \
	remove_element.c \
	free_redir.c \
	message_redir.c \
	my_strncpy2.c \
	redirection.c \
	several_pipes.c

SRCS2	= count_num.c \
	  convert_base.c \
	  my_charisalpha.c \
	  my_charisnum.c \
	  my_getnbr.c \
	  my_putchar.c \
	  my_putcharerror.c \
	  my_putnbr.c \
	  my_putstr.c \
	  my_putstrerror.c \
	  my_revstr.c \
	  my_show_wordtab.c \
	  my_strcapitalize.c \
	  my_strcat.c \
	  my_strcmp.c \
	  my_strcpy.c \
	  my_strdup.c \
	  my_str_isalpha.c \
	  my_str_islower.c \
	  my_str_isnum.c \
	  my_str_isprintable.c \
	  my_str_isupper.c \
	  my_str_to_wordtab.c \
	  my_strlen.c \
	  my_strlowcase.c \
	  my_strncat.c \
	  my_strncmp.c \
	  my_strncpy.c \
	  my_strnum_to_wordtab.c \
	  my_strstr.c \
	  my_strupcase.c \
	  my_swap.c \
	  pmalloc.c \
	  pow_10.c \
	  power.c \
	  get_next_line.c

SRCSLIB	= $(addprefix $(LIB), $(SRCS2))

OBJS	= $(SRCS:.c=.o)

OBJSLIB	= $(SRCSLIB:.c=.o)


all: $(LIBNAME) $(NAME)

$(NAME): $(OBJS)
	$(CC) $(OBJS) -o $(NAME) $(LDFLAGS)

$(LIBNAME): $(OBJSLIB)
	make -C $(LIB)
clean:
	$(RM) $(OBJS)
	make clean -C $(LIB)

fclean: clean
	$(RM) $(NAME)
	make fclean -C $(LIB)

re: fclean all

.PHONY: all clean fclean re
