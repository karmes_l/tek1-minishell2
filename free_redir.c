/*
** free_redir.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell2/v1/minishell2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Mar  2 19:11:54 2015 lionel karmes
** Last update Sat Mar  7 14:41:33 2015 lionel karmes
*/

#include "my.h"

void	free_redir(t_redir *redir)
{
  remove_list(&(redir->list_openfile));
  remove_list(&(redir->list_bin));
}
