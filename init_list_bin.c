/*
** init_redir.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell2/v1/minishell2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Feb 26 18:32:50 2015 lionel karmes
** Last update Sun Mar 15 15:13:57 2015 lionel karmes
*/

#include "my.h"

int		add_bin(t_list **list_bin, char *path)
{
  t_bin		*bin;
  static int	id = 0;

  if ((*list_bin)->size == 0)
    id = 0;
  id++;
  if (!(bin = pmalloc(sizeof(t_bin))))
    return (-1);
  bin->id = id;
  bin->name = path;
  bin->sortie = NULL;
  bin->entry = NULL;
  bin->double_left = NULL;
  my_put_in_list_end(list_bin, bin);
  return (1);
}

char	*load_path(char *cmds, int i, int len_path)
{
  int	j;
  char	*path;

  if (!(path = pmalloc(sizeof(char) * (len_path + 1))))
    return (NULL);
  j = 0;
  if (i > 0 && space_or_tab(cmds[i - 1]))
    i--;
  while (j < len_path)
    {
      path[j] = cmds[i - len_path + j];
      j++;
    }
  path[j] = '\0';
  return (path);
}

int	calc_len_path(char *cmds, int *i)
{
  int	len_path;

  len_path = 0;
  while (!char_redirection(cmds[*i + len_path]) && cmds[*i + len_path])
    len_path++;
  (*i) += len_path;
  if (len_path > 0 && space_or_tab(cmds[*i - 1]))
    len_path--;
  return (len_path);
}

int	synthax_inv_bin(t_list **list_bin, char *cmds, int *i)
{
  int	len_path;
  char	*path;

  (*i)++;
  if (cmds[*i] == '>' || cmds[*i] == '<')
    (*i)++;
  if (space_or_tab(cmds[*i]))
    (*i)++;
  while (!space_or_tab(cmds[*i]) && cmds[*i])
    (*i)++;
  if (cmds[*i] == '\0')
    return (1);
  (*i)++;
  len_path = calc_len_path(cmds, i);
  if ((path = load_path(cmds, *i, len_path)) == NULL)
    return (-1);
  if (add_bin(list_bin, path) == -1)
    return (-1);
  return (1);
}

int	init_list_bin_next(t_list **list_bin, char *cmds, int *i)
{
  int	len_path;
  char	*path;

  if (cmds[*i] == '\0')
    return (1);
  len_path = calc_len_path(cmds, i);
  if (len_path == 0 && cmds[*i] != '|')
    {
      if (synthax_inv_bin(list_bin, cmds, i) == -1)
	return (-1);
    }
  else
    {
      if ((path = load_path(cmds, *i, len_path)) == NULL)
	return (-1);
      if (add_bin(list_bin, path) == -1)
	return (-1);
    }
  return (1);
}
