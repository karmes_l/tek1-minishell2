/*
** init_redir.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell2/v1/minishell2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Feb 26 18:32:50 2015 lionel karmes
** Last update Sun Mar 15 15:16:10 2015 lionel karmes
*/

#include "my.h"

int	init_list_openfile(t_list **list_openfile, char *cmds)
{
  int		i;
  int		option;

  i = 0;
  while (cmds[i])
    {
      while (cmds[i] != '<' && cmds[i] != '>' && cmds[i])
	i++;
      if (cmds[i] == '\0')
	break;
      option = option_open(cmds, i);
      i++;
      if (cmds[i] == '>' || cmds[i] == '<')
	i++;
      if (option == DOUBLE_LEFT)
	  continue;
      if (space_or_tab(cmds[i]))
	i++;
      if (init_list_openfile_next(list_openfile, cmds, i, option) == -1)
	return (-1);
    }
  return (1);
}

int	init_list_bin(t_list **list_bin, char **cmds)
{
  int	i;

  i = 0;
  if (init_list_bin_next(list_bin, *cmds, &i) == -1)
    return (-1);
  while ((*cmds)[i])
    {
      while ((*cmds)[i] != '|' && (*cmds)[i])
	i++;
      if ((*cmds)[i] == '|')
	i++;
      if (space_or_tab((*cmds)[i]))
	i++;
      if (init_list_bin_next(list_bin, *cmds, &i) == -1)
	return (-1);
    }
  return (1);
}

int		valid_openfile(t_list *list_openfile)
{
  t_openfile	*openfile;

  openfile = list_openfile->l_start;
  while (openfile != NULL)
    {
      if (open(openfile->path, openfile->option, 00644) == -1)
	return (msg_error_open(openfile->path, openfile->option));
      openfile = openfile->e_next;
    }
  return (1);
}

int	init_redir(t_redir *redir, char **commands)
{
  redir->list_openfile = new_list();
  redir->list_bin = new_list();
  redir->list_openfile->what_element = OPENFILE;
  redir->list_bin->what_element = BIN;
  if (valid_cmds(commands) == -1)
    return (-1);
  if (init_list_openfile(&(redir->list_openfile), *commands) == -1)
    return (-1);
  if (valid_openfile(redir->list_openfile) == -1)
    return (-1);
  if (init_list_bin(&(redir->list_bin), commands) == -1)
    return (-1);
   coordination_bin_openfile(redir->list_bin->l_start,
  			    redir->list_openfile->l_start, *commands);
  return (1);
}
