/*
** init_redir.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell2/v1/minishell2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Feb 26 18:32:50 2015 lionel karmes
** Last update Sun Mar 15 15:14:25 2015 lionel karmes
*/

#include "my.h"

int	valid_redirection(char *cmds)
{
 int	i;

  i = 0;
  if (cmds[0] == '|')
    return (msg_error_redirection(cmds[0]));
  while (cmds[i])
    {
      while (!char_redirection(cmds[i]) && cmds[i])
	i++;
      if (cmds[i] == '|')
	i++;
      if (cmds[i] == '<' || cmds[i] == '>')
	i++;
      if (i > 0 && (cmds[i - 1] == '<' || cmds[i - 1] == '>') &&
	  cmds[i] == cmds[i - 1])
	i++;
      if (space_or_tab(cmds[i]))
	i++;
      if (char_redirection(cmds[i]) ||
	  (i > 0 && (cmds[i - 1] == '<' ||
		     cmds[i - 1] == '>') && cmds[i] == '\0'))
	return (msg_error_redirection(cmds[i]));
    }
  return (1);
}

int	update_cmds(char **cmds)
{
  char	*buff;

  while (1)
    {
      my_putstr("> ");
      read_signal();
      buff = get_next_line(0);
      if (g_signal_int == SIGINT || g_signal_int == SIGTSTP ||
	  g_signal_int == SIGQUIT)
	{
	  my_putchar('\n');
	  return (-1);
	}
      epure_str(buff);
      if (buff == NULL || (buff != NULL && buff[0] != '\0'))
      	break;
    }
  if (buff == NULL)
    return (msg_error_redirection_str(buff));
  if (valid_redirection(buff) == -1)
    return (-1);
  if (!(*cmds = char1_tab_realloc(*cmds, my_strlen(*cmds) + my_strlen(buff))))
    return (-1);
  my_strcat(*cmds, buff);
  return (1);
}

int	valid_cmds(char **cmds)
{
  if (valid_redirection(*cmds) == -1)
    return (-1);
  while (my_strlen(*cmds) > 0 && (*cmds)[my_strlen(*cmds) - 1] == '|')
    {
      if (update_cmds(cmds) == -1)
	return (-1);
    }
  return (1);
}
