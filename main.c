/*
** main.c for  in /home/karmes_l/Projets/Systeme_Unix/my_select
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Dec 27 10:41:25 2014 lionel karmes
** Last update Sun Jan 11 17:33:06 2015 lionel karmes
*/

#include "my.h"

int	main(int argc, char **argv, char **env)
{
  (void) argv;
  if (argc < 2)
    minishell1(env);
  else
    my_putstrerror("Not arguments required\n");
  return (0);
}
