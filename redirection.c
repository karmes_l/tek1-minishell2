/*
** redirection.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell2/v1/minishell2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Mar 10 14:55:59 2015 lionel karmes
** Last update Sun Mar 15 15:17:02 2015 lionel karmes
*/

#include "my.h"

void	redirection_double_left_next(int fildes[2], char *buff)
{
  if (buff != NULL)
    {
      write(fildes[1], buff, my_strlen(buff));
      write(fildes[1], "\n", 1);
      free(buff);
    }
}

int	redirection_double_left(char *end)
{
  char	*buff;
  int	fildes[2];

  if (init_pipe(fildes) == -1)
    return (-1);
  while (1)
    {
      my_putstr("> ");
      read_signal();
      buff = get_next_line(0);
      if (g_signal_int == SIGINT || g_signal_int == SIGQUIT ||
	  g_signal_int == SIGTSTP)
	{
	  my_putchar('\n');
	  return (-1);
	}
      if (buff == NULL ||(buff != NULL && !my_strcmp(end, buff)))
      	break;
      redirection_double_left_next(fildes, buff);
    }
  close(fildes[1]);
  dup2(fildes[0], 0);
  close(fildes[0]);
  return (1);
}

int	redirection(t_bin *bin)
{
  int	fd;

  if (bin->entry != NULL)
    {
      if ((fd = open(bin->entry->path, bin->entry->option)) == -1)
	return (-1);
      dup2(fd, 0);
      close (fd);
    }
  if (bin->double_left != NULL)
    if (redirection_double_left(bin->double_left) == -1)
      return (-1);
  if (bin->sortie != NULL)
    {
      if ((fd = open(bin->sortie->path, bin->sortie->option)) == -1)
	return (-1);
      dup2(fd, 1);
      close (fd);
    }
  if (bin->entry != NULL || bin->double_left != NULL || bin->sortie != NULL)
    return (1);
  return (0);
}
